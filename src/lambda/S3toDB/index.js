let config = require("config")
let AWS = require("aws-sdk");
let rekognition = new AWS.Rekognition();
let ddb = new AWS.DynamoDB();
let id;

exports.handler = function(event, context, callback) {
  let bucket = event.Records[0].s3.bucket.name;
  let key = event.Records[0].s3.object.key;
  id = key.split("/")[1].split("-")[0];
  rekognizeFace(bucket, key)
    .then(function(data) {
      return addToFacesTable(data["FaceDetails"]);
    }).then(function(data) {
      console.log("Data added to " + config.dynamo.tableName + " Table");
      callback(null, data);
    }).catch(function(err) {
      callback(err, null);
    });
};

function addToFacesTable(faceDetails) {
  /*"HAPPY"
  "SAD"
  "ANGRY"
  "CONFUSED"
  "DISGUSTED"
  "SURPRISED"
  "CALM"
  "UNKNOWN" */
  let emotions = faceDetails[0]["Emotions"];
  let emotionData = {}
  emotions.map(function(emotion) {
      emotionData[emotion.Type] = emotion.Confidence;
  })

  let params = {
    TableName: config.dynamo.tableName,
    Item: {
      id: {N: String(id)},
      timestamp: {S: String(new Date().getTime())},
      HAPPY: {S: String(emotionData.HAPPY)},
      SAD: {S: String(emotionData.SAD)},
      ANGRY: {S: String(emotionData.ANGRY)},
      CONFUSED: {S: String(emotionData.CONFUSED)},
      DISGUSTED: {S: String(emotionData.DISGUSTED)},
      SURPRISED: {S: String(emotionData.SURPRISED)},
      CALM: {S: String(emotionData.CALM)},
      UNKNOWN: {S: String(emotionData.UNKNOWN)}
    }
  };
  return ddb.putItem(params).promise();
}

function rekognizeFace(bucket, key) {
  let params = {
    Attributes: ["ALL"],
    Image: {
      S3Object: {
        Bucket: bucket,
        Name: key
      }
    }
  };
  return rekognition.detectFaces(params).promise();
}
