import Vue from 'vue'
import Router from 'vue-router'
import VeeValidate from 'vee-validate'
import Form from '@/components/Form.vue'
import LandingPage from '@/components/LandingPage.vue'
import VueResource from 'vue-resource';
import Results from '@/components/Results.vue'
import Trend from 'vuetrend';

Vue.use(Trend);
Vue.use(Router);
Vue.use(VeeValidate);
Vue.use(VueResource);

export default new Router({
  mode: 'history',
  base: window.location.pathName,
  routes: [
    {
      path: '/demo',
      name: 'Demo',
      component: Form
    },
    {
    	path: '/',
    	name: 'Home',
    	component: LandingPage
    },
    {
      path: '/results',
      name: 'Results',
      component: Results
    }
  ]
})
